import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    timerList: []
  },
  mutations: {
    setToTimerList: (state, timer) => state.timerList.push(timer),
    updateTimer: (state, val) => state.timerList = val,
    changeTimerState: (state, [idx, val]) => state.timerList.splice(idx, 1, val),
    deleteTimer: (state, idx) => state.timerList.splice(idx, 1)
  },
})
